package com.example.sample;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.content.res.Resources;

public class CalendarUtils {

    static private class CalendarTerms {
        public static String year;
//        public static String month;
        public static String day;
        public static String hour;
        public static String minute;
        public static String second;
        public static String now;
    }

    static public class CalendarDiff {
        public boolean future;
        public long milliseconds;
        public long seconds;
        public long minutes;
        public long hours;
        public long days;
        public long years;

        @Override
        public String toString() {
            if (years > 0) return years + CalendarTerms.year;
            if (days > 0) return days + CalendarTerms.day;
            if (hours > 0) return hours + CalendarTerms.hour;
            if (minutes > 0) return minutes + CalendarTerms.minute;
            if (seconds > 0) return seconds + CalendarTerms.second;
            return CalendarTerms.now;
        }
    }
    
    static private String actionString = "Opened"; // move to @strings

    static private final long MILLIS_IN_SECOND = 1000L;
    static private final long MILLIS_IN_MINUTE = 60000L;
    static private final long MILLIS_IN_HOUR   = 3600000L;
    static private final long MILLIS_IN_DAY    = 86400000L;
    static private final long MILLIS_IN_YEAR   = 31556908800L; // DAY * 365.242
    static private DateFormat mDateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
    static {
        mDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
    }

    static public String convertToGmtString(Calendar cal) {
        return mDateFormat.format(cal.getTime());
    }

    /**
     * Returns a date String (without the time) according to the default locale 
     * @param cal
     * @return
     */
    static public String convertToDateString(Calendar cal) {
        return DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault()).format(cal.getTime());
    }

    /**
     * Returns a date + time String according to the default locale 
     * @param cal
     * @return
     */
    static public String convertToDateTimeString(Calendar cal) {
        return DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, Locale.getDefault()).format(cal.getTime());
    }

    static public Date convertToDate(String timestamp) throws ParseException {
        Date date = null;
        try {
            date = mDateFormat.parse(timestamp);
        } catch (ParseException e) {
            // try a different format (stupid inconsistency in the server)
            DateFormat alternateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm");
            alternateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

            date = alternateFormat.parse(timestamp);
        }
        
        return date;
    }
    
    static public CalendarDiff getCalendarsDiff(Calendar cal) {
        CalendarDiff diff = new CalendarDiff();

        Calendar now = Calendar.getInstance();
        long d = cal.getTimeInMillis() - now.getTimeInMillis();
        
        // check if the given date is in the past or the future
        diff.future = d > 0;
        d = Math.abs(d);
        
        diff.milliseconds = d;
        
        diff.years = d / MILLIS_IN_YEAR;
        d -= diff.years * MILLIS_IN_YEAR;
        
        diff.days = d / MILLIS_IN_DAY;
        d -= diff.days * MILLIS_IN_DAY;

        diff.hours = d / MILLIS_IN_HOUR;
        d -= diff.hours * MILLIS_IN_HOUR;
        
        diff.minutes = d / MILLIS_IN_MINUTE;
        d -= diff.minutes * MILLIS_IN_MINUTE;
        
        diff.seconds = d / MILLIS_IN_SECOND;
        
        return diff;
    }
    
    static private String getTimeToFromDate(Date date) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        return getTimeToFromDate(cal);
    }

    static private String getTimeToFromDate(Calendar cal) {

        CalendarDiff diff = getCalendarsDiff(cal);

        if (diff.years > 0) {
            if (diff.future) {
                return "Will open in " + diff.years + " years";
            } else {
                return actionString + diff.years + " years ago";
            }
        }

        if (diff.days > 0) {
            if (diff.future) {
                return "Will open in " + diff.days + " days";
            } else {
                return actionString + diff.days + " days ago";
            }
        }

        if (diff.hours > 0) {
            if (diff.future) {
                return "Will open in " + diff.hours + " hours";
            } else {
                return actionString + diff.hours + " hours ago";
            }
        }

        if (diff.minutes > 0) {
            if (diff.future) {
                return "Will open in " + diff.minutes + " minutes";
            } else {
                return actionString + diff.minutes + " minutes ago";
            }
        }

        if (diff.seconds > 0) {
            if (diff.future) {
                return "Will open in " + diff.seconds + " seconds";
            } else {
                return actionString + diff.seconds + " seconds ago";
            }
        }
        
        return "Capsule is open";
    }

    static public String getTimeSinceOpenedFromDate(Date date)
    {
        actionString = "Opened ";
        return getTimeToFromDate(date);
    }

    static public String getTimeSinceCreatedFromDate(Date date)
    {
        actionString = "Created ";
        return getTimeToFromDate(date);
    }

    static public String getTimeSinceOpenedFromDate(Calendar cal)
    {
        actionString = "Opened ";
        return getTimeToFromDate(cal);
    }

    static public String getTimeSinceCreatedFromDate(Calendar cal)
    {
        actionString = "Created ";
        return getTimeToFromDate(cal);
    }
}

