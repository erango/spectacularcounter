package com.example.sample;

import java.util.Calendar;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Counter counter = (Counter) findViewById(R.id.counter);

        Calendar target = Calendar.getInstance();
//        target.add(Calendar.YEAR, 2);
        target.add(Calendar.DAY_OF_YEAR, 1);
//        target.add(Calendar.SECOND, -45);
//        target.add(Calendar.MINUTE, -58);
//        target.add(Calendar.HOUR, -22);

        counter.setTargetDate(target);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
