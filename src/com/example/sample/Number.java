package com.example.sample;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Number extends LinearLayout {

    static public interface CounterNumberListener {

        /**
         * Called when the number goes below 0 and wraps around to {@link Number#mMaxNumber}
         * @param view
         */
        public void onPassZero(Number view);
    }

    static private final float SIDE_TEXT_SIZE_PERC = .6f;

    private TextView mPrefixTextView;
    private TextView mSuffixTextView;
    private ArrayList<Digit> mDigits;

    int mMaxNumber;
    private int mNumberOfDigits;
    private String mPrefix;
    private String mSuffix;

    int mNumber;

    CounterNumberListener mListener;

    public Number(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CounterNumber, 0, 0);

        try {
            mMaxNumber = a.getInteger(R.styleable.CounterNumber_maxNumber, 0);
            mPrefix = a.getString(R.styleable.CounterNumber_prefix);
            mSuffix = a.getString(R.styleable.CounterNumber_suffix);
        } finally {
            a.recycle();
        }

        mNumberOfDigits = String.valueOf(mMaxNumber).length();

        init(context);
    }

    public Number(Context context, AttributeSet attrs, int defStyle) {
        this(context, attrs);
    }

    public void setListener(CounterNumberListener listener) {
        mListener = listener;
    }

    private void init(Context context) {
        // setBaselineAligned(true);
        setGravity(Gravity.CENTER_HORIZONTAL);

        if (mNumberOfDigits == 0) {
            mNumberOfDigits = 1;
        }

        if (mPrefix != null) {
            mPrefixTextView = new TextView(context);
            mPrefixTextView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            mPrefixTextView.setText(mPrefix);
            addView(mPrefixTextView);
        }

        if (mDigits == null) {
            mDigits = new ArrayList<Digit>();
        }

        for (int i = 0; i < mNumberOfDigits; i++) {
            final Digit digit = new Digit(getContext());
            digit.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            addView(digit);
            mDigits.add(digit);
        }

        if (mSuffix != null) {
            mSuffixTextView = new TextView(context);
            mSuffixTextView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            mSuffixTextView.setText(mSuffix);
            mSuffixTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            addView(mSuffixTextView);
        }
    }

    public void setTextSize(float textSize) {
        for (int i = 0; i < mNumberOfDigits; i++) {
            mDigits.get(i).setTextSize(textSize);
        }

        if (mPrefixTextView != null) {
            mPrefixTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize * SIDE_TEXT_SIZE_PERC);
        }

        if (mSuffixTextView != null) {
            mSuffixTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX  , textSize * SIDE_TEXT_SIZE_PERC);
        }
    }

    public void setTextColor(int textColor) {

        for (int i = 0; i < mNumberOfDigits; i++) {
            mDigits.get(i).setTextColor(textColor);
        }

        if (mPrefixTextView != null) {
            mPrefixTextView.setTextColor(textColor);
        }

        if (mSuffixTextView != null) {
            mSuffixTextView.setTextColor(textColor);
        }
    }

    public void setNumber(int number) {
        if (number < 0) {
            throw new IllegalArgumentException();
        }

        mNumber = number;

        // initialize all digits
        for (int i = mNumberOfDigits - 1; i >= 0; --i) {
            mDigits.get(i).setDigit(number % 10, false);

            number /= 10;
        }
    }

    public void decrement() {
        boolean wentPastZero = false;

        --mNumber;
        if (mNumber == -1) {
            mNumber = mMaxNumber;
            wentPastZero = true;
        }

        int number = mNumber;
        for (int i = mNumberOfDigits - 1; i >= 0; --i) {
            mDigits.get(i).setDigit(number % 10, true);

            number /= 10;
        }

        if (wentPastZero && mListener != null) {
            mListener.onPassZero(this);
        }
    }

}
