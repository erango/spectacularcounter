package com.example.sample;

import java.util.Calendar;

import com.example.sample.CalendarUtils.CalendarDiff;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

public class Counter extends LinearLayout
                     implements Number.CounterNumberListener {

    public interface CountdownListener {
        void onCountdownOver();
    }

    static private final int DEFAULT_TEXT_SIZE = 80;
    static private final int DEFAULT_TEXT_COLOR = 0xff000000;

    View mYearsSpaceFiller;
    Number mYears;
    Number mDays;
    Number mHours;
    Number mMinutes;
    Number mSeconds;

    float mTextSize;
    int mTextColor;

    Calendar mTargetDate = null;
    CountDownTimer mCountDownTimer;

    CountdownListener mListener;

    public Counter(Context context, AttributeSet attrs) {
        super(context, attrs);

        final int[] ATTRS = new int[] {android.R.attr.textSize, android.R.attr.textColor};

        TypedArray a = context.obtainStyledAttributes(attrs, ATTRS);
        mTextSize = a.getDimensionPixelSize(0, DEFAULT_TEXT_SIZE);
        mTextColor = a.getColor(1, DEFAULT_TEXT_COLOR);
        a.recycle();

        init(context);
    }

    public Counter(Context context) {
        super(context);
        init(context);
    }

    public Counter(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        startCounter();
    }

    public void setListener(CountdownListener listener) {
        mListener = listener;
    }

    private void init(Context context) {
        View root = inflate(context, R.layout.view_counter, this);

        mYearsSpaceFiller = root.findViewById(R.id.yearsSpaceFiller);
        mYears = (Number) root.findViewById(R.id.yearView);
        mDays = (Number) root.findViewById(R.id.dayView);
        mHours = (Number) root.findViewById(R.id.hourView);
        mMinutes = (Number) root.findViewById(R.id.minuteView);
        mSeconds = (Number) root.findViewById(R.id.secondView);

        mYears.setTextSize(mTextSize);
        mDays.setTextSize(mTextSize);
        mHours.setTextSize(mTextSize);
        mMinutes.setTextSize(mTextSize);
        mSeconds.setTextSize(mTextSize);

        mYears.setTextColor(mTextColor);
        mDays.setTextColor(mTextColor);
        mHours.setTextColor(mTextColor);
        mMinutes.setTextColor(mTextColor);
        mSeconds.setTextColor(mTextColor);

        mDays.setListener(this);
        mHours.setListener(this);
        mMinutes.setListener(this);
        mSeconds.setListener(this);
    }

    public void increment() {
        throw new RuntimeException("Not implemented");
    }

    private void decrement() {
        mSeconds.decrement();
    }

    @Override
    public void onPassZero(Number view) {

        Number number = null;
        switch (view.getId()) {
        case R.id.secondView:
            number = mMinutes;
            break;
        case R.id.minuteView:
            number = mHours;
            break;
        case R.id.hourView:
            number = mDays;
            break;
        case R.id.dayView:
            number = mYears;
            break;
        default:
            break;
        }

        if (number != null) {
            number.decrement();
        }
    }

    public void setTargetDate(Calendar targetCalendar) {
        mTargetDate = targetCalendar;

        startCounter();
    }

    private void startCounter() {
        if (mTargetDate == null) {
            return;
        }

        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }

        CalendarDiff diff = CalendarUtils.getCalendarsDiff(mTargetDate);

        if (diff.years != 0) {
            mYears.setNumber((int) diff.years);
            mYears.setVisibility(View.VISIBLE);
            mYearsSpaceFiller.setVisibility(View.VISIBLE);
        }
        mDays.setNumber((int) diff.days);
        mHours.setNumber((int) diff.hours);
        mMinutes.setNumber((int) diff.minutes);
        mSeconds.setNumber((int) diff.seconds);

        mCountDownTimer = new CountDownTimer(diff.milliseconds, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                decrement();
            }

            @Override
            public void onFinish() {
                if (mListener != null) {
                    mListener.onCountdownOver();
                }
            }
        };

        mCountDownTimer.start();
    }

}
