package com.example.sample;

import android.content.Context;
import android.util.TypedValue;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Digit extends LinearLayout {

    static final private int NORMAL_ANIMATION_DUARATION = 500;

    TextView digit1;
    TextView digit2;

    private int mBaseline;
    int mCurrentDigit;
    int mDigitInLastLayout = -1;

    public Digit(Context context) {
        super(context);

        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        // view size should be that of a single digit, so measure the text view
        // accordingly (but restore previous text after measure)
        CharSequence a = digit1.getText();
        digit1.setText("0");
        measureChild(digit1, widthMeasureSpec, heightMeasureSpec);
        mBaseline = digit1.getBaseline();
        digit1.setText(a);

        a = digit2.getText();
        digit2.setText("0");
        measureChild(digit2, widthMeasureSpec, heightMeasureSpec);
        digit2.setText(a);

        // set the view to be the size of the measured TextView
        setMeasuredDimension(resolveSize(digit1.getMeasuredWidth(), widthMeasureSpec),
                resolveSize(digit1.getMeasuredHeight(), heightMeasureSpec));
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        // this test prevents adjacent digits from flickering
        if (mDigitInLastLayout != mCurrentDigit) {
            super.onLayout(changed, l, t, r, b);
            mDigitInLastLayout = mCurrentDigit;
        }
    }

    private void init() {
        this.setOrientation(LinearLayout.VERTICAL);

        digit1 = new TextView(getContext());
        digit2 = new TextView(getContext());

        digit1.setText("0");
        digit2.setText("0");

        addView(digit1);
        addView(digit2);
    }

    public void setTextSize(float textSize) {
        digit1.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        digit2.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
    }

    public void setTextColor(int textColor) {
        digit1.setTextColor(textColor);
        digit2.setTextColor(textColor);
    }

    public void setDigit(int newDigit, boolean animate) {

        if (newDigit == mCurrentDigit) {
            return;
        }

        mCurrentDigit = newDigit;

        if (!animate) {
            digit1.setText(String.valueOf(newDigit));
            return;
        }

        digit2.setY(getHeight());
        digit2.setText(String.valueOf(newDigit));

        TranslateAnimation ta = new TranslateAnimation(0, 0, 0, -getHeight());
        ta.setFillAfter(true);
        ta.setDuration(NORMAL_ANIMATION_DUARATION);

        TranslateAnimation ta2 = new TranslateAnimation(0, 0, 0, -getHeight());
        ta2.setFillAfter(true);
        ta2.setDuration(NORMAL_ANIMATION_DUARATION);

        ta.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                digit1.setText(digit2.getText());
                digit1.setY(0);
                digit2.setY(getHeight());
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

        });

        digit1.startAnimation(ta);
        digit2.startAnimation(ta2);
    }

    @Override
    public int getBaseline() {
        return mBaseline;
    }

}
